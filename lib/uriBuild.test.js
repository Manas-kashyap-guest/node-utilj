'use strict';

var _index = require('./index');

describe('uriBuild()', function () {
  var scheme = 'http:',
      host = 'my.happy.world',
      port = 3222,
      path = 'find/us',
      pathParams = {
    country: 'Russia',
    town: 'Sevastopol'
  },
      query = {
    time: '333',
    locale: 'ru'
  };

  it('should build scheme with host', function () {
    expect((0, _index.uriBuild)({ scheme: scheme, host: host })).toEqual('http://my.happy.world');
  });

  it('should build scheme with host and port', function () {
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port })).toEqual('http://my.happy.world:3222');
  });

  it('should error out when scheme provided without host', function () {
    expect(function () {
      return (0, _index.uriBuild)({ scheme: scheme });
    }).toThrowError();
  });

  it('should build with host only', function () {
    expect((0, _index.uriBuild)({ host: host })).toEqual('my.happy.world');
  });

  it('should build with host and port', function () {
    expect((0, _index.uriBuild)({ host: host, port: port })).toEqual('my.happy.world:3222');
  });

  it('should build with path only', function () {
    expect((0, _index.uriBuild)({ path: path })).toEqual('find/us');
    expect((0, _index.uriBuild)({ path: '/find/us' })).toEqual('/find/us');
    expect((0, _index.uriBuild)({ path: '/find/us/' })).toEqual('/find/us');
  });

  it('should build with parametrized path', function () {
    expect((0, _index.uriBuild)({ path: 'find/us/:country/:town', pathParams: pathParams })).toEqual('find/us/Russia/Sevastopol');
    expect((0, _index.uriBuild)({ path: '/find/us/:country/:town', pathParams: pathParams })).toEqual('/find/us/Russia/Sevastopol');
    expect((0, _index.uriBuild)({ path: '/find/us/:country/:town/', pathParams: pathParams })).toEqual('/find/us/Russia/Sevastopol');
  });

  it('should error out when no params passed with parametrized path', function () {
    expect(function () {
      return (0, _index.uriBuild)({ path: '/find/us/:country/:town' });
    }).toThrowError();
  });

  it('should build with mixed host, port and path', function () {
    expect((0, _index.uriBuild)({ host: host, path: path })).toEqual('my.happy.world/find/us');
    expect((0, _index.uriBuild)({ host: host, path: '/find/us' })).toEqual('my.happy.world/find/us');
    expect((0, _index.uriBuild)({ host: host, path: '/find/us/' })).toEqual('my.happy.world/find/us');
    expect((0, _index.uriBuild)({ host: host, port: port, path: path })).toEqual('my.happy.world:3222/find/us');
    expect((0, _index.uriBuild)({ host: host, port: port, path: '/find/us' })).toEqual('my.happy.world:3222/find/us');
    expect((0, _index.uriBuild)({ host: host, port: port, path: '/find/us/' })).toEqual('my.happy.world:3222/find/us');
    expect((0, _index.uriBuild)({ host: host, port: port, path: 'find/us/:country/:town', pathParams: pathParams })).toEqual('my.happy.world:3222/find/us/Russia/Sevastopol');
    expect((0, _index.uriBuild)({ host: host, port: port, path: '/find/us/:country/:town', pathParams: pathParams })).toEqual('my.happy.world:3222/find/us/Russia/Sevastopol');
    expect((0, _index.uriBuild)({ host: host, port: port, path: '/find/us/:country/:town/', pathParams: pathParams })).toEqual('my.happy.world:3222/find/us/Russia/Sevastopol');
  });

  it('should build with mixed scheme, host, port and path', function () {
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, path: path })).toEqual('http://my.happy.world/find/us');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, path: '/find/us' })).toEqual('http://my.happy.world/find/us');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, path: '/find/us/' })).toEqual('http://my.happy.world/find/us');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: path })).toEqual('http://my.happy.world:3222/find/us');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: '/find/us' })).toEqual('http://my.happy.world:3222/find/us');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: '/find/us/' })).toEqual('http://my.happy.world:3222/find/us');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: 'find/us/:country/:town', pathParams: pathParams })).toEqual('http://my.happy.world:3222/find/us/Russia/Sevastopol');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: '/find/us/:country/:town', pathParams: pathParams })).toEqual('http://my.happy.world:3222/find/us/Russia/Sevastopol');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: '/find/us/:country/:town/', pathParams: pathParams })).toEqual('http://my.happy.world:3222/find/us/Russia/Sevastopol');
  });

  it('should build query only', function () {
    expect((0, _index.uriBuild)({ query: query })).toEqual('time=333&locale=ru');
  });

  it('should build path with query', function () {
    expect((0, _index.uriBuild)({ path: path, query: query })).toEqual('find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ path: '/find/us', query: query })).toEqual('/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ path: '/find/us/', query: query })).toEqual('/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ path: 'find/us/:country/:town', pathParams: pathParams, query: query })).toEqual('find/us/Russia/Sevastopol?time=333&locale=ru');
    expect((0, _index.uriBuild)({ path: 'find/us/:country/:town/', pathParams: pathParams, query: query })).toEqual('find/us/Russia/Sevastopol?time=333&locale=ru');
    expect((0, _index.uriBuild)({ path: '/find/us/:country/:town/', pathParams: pathParams, query: query })).toEqual('/find/us/Russia/Sevastopol?time=333&locale=ru');
  });

  it('should build with mixed host, port and query', function () {
    expect((0, _index.uriBuild)({ host: host, query: query })).toEqual('my.happy.world?time=333&locale=ru');
    expect((0, _index.uriBuild)({ host: host, port: port, query: query })).toEqual('my.happy.world:3222?time=333&locale=ru');
  });

  it('should build with mixed scheme, host, port and query', function () {
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, query: query })).toEqual('http://my.happy.world?time=333&locale=ru');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, query: query })).toEqual('http://my.happy.world:3222?time=333&locale=ru');
  });

  it('should build with mixed host, port, path and query', function () {
    expect((0, _index.uriBuild)({ host: host, path: path, query: query })).toEqual('my.happy.world/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ host: host, path: '/find/us', query: query })).toEqual('my.happy.world/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ host: host, path: '/find/us/', query: query })).toEqual('my.happy.world/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ host: host, port: port, path: path, query: query })).toEqual('my.happy.world:3222/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ host: host, port: port, path: '/find/us', query: query })).toEqual('my.happy.world:3222/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ host: host, port: port, path: '/find/us/', query: query })).toEqual('my.happy.world:3222/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ host: host, port: port, path: 'find/us/:country/:town', pathParams: pathParams, query: query })).toEqual('my.happy.world:3222/find/us/Russia/Sevastopol?time=333&locale=ru');
    expect((0, _index.uriBuild)({ host: host, port: port, path: '/find/us/:country/:town', pathParams: pathParams, query: query })).toEqual('my.happy.world:3222/find/us/Russia/Sevastopol?time=333&locale=ru');
    expect((0, _index.uriBuild)({ host: host, port: port, path: '/find/us/:country/:town/', pathParams: pathParams, query: query })).toEqual('my.happy.world:3222/find/us/Russia/Sevastopol?time=333&locale=ru');
  });

  it('should build with mixed scheme, host, port, path and query', function () {
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, path: path, query: query })).toEqual('http://my.happy.world/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, path: '/find/us', query: query })).toEqual('http://my.happy.world/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, path: '/find/us/', query: query })).toEqual('http://my.happy.world/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: path, query: query })).toEqual('http://my.happy.world:3222/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: '/find/us', query: query })).toEqual('http://my.happy.world:3222/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: '/find/us/', query: query })).toEqual('http://my.happy.world:3222/find/us?time=333&locale=ru');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: 'find/us/:country/:town', pathParams: pathParams, query: query })).toEqual('http://my.happy.world:3222/find/us/Russia/Sevastopol?time=333&locale=ru');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: '/find/us/:country/:town', pathParams: pathParams, query: query })).toEqual('http://my.happy.world:3222/find/us/Russia/Sevastopol?time=333&locale=ru');
    expect((0, _index.uriBuild)({ scheme: scheme, host: host, port: port, path: '/find/us/:country/:town/', pathParams: pathParams, query: query })).toEqual('http://my.happy.world:3222/find/us/Russia/Sevastopol?time=333&locale=ru');
  });
});