'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});


var missingSettingError = function missingSettingError(setting) {
  return new Error('uriBuild: valid \'' + setting + '\' setting is required in such input');
};

var rptrim = function rptrim(path) {
  if (path[path.length - 1] === '/') return path.substr(0, path.length - 1);
  return path;
};

var lptrim = function lptrim(path) {
  if (path[0] === '/') return path.substr(1);
  return path;
};

exports.default = function (params) {
  var scheme = params.scheme,
      host = params.host,
      port = params.port,
      path = params.path,
      pathParams = params.pathParams,
      query = params.query,
      template = '{scheme}{host}{port}{path}{query}',
      parts = {
    scheme: '',
    host: '',
    port: '',
    path: '',
    query: ''
  };


  if (scheme) {
    if (!host) throw missingSettingError('host');
    parts.scheme = scheme.replace(':', '') + '://';
    parts.host = host;
    if (port) parts.port = ':' + port;
  }

  if (host) {
    parts.host = host;
    if (port) parts.port = ':' + port;
  }

  if (path) {
    var finalPath = void 0;
    if (path.indexOf(':') !== -1) {
      if (!pathParams) throw missingSettingError('pathParams');
      // append unparametrized path
      finalPath = path.replace(new RegExp(':(' + Object.keys(pathParams).join('|') + ')', 'g'), function (_, p) {
        return pathParams[p];
      });
    } else {
      finalPath = path;
    }
    finalPath = rptrim(finalPath);
    if (host) finalPath = '/' + lptrim(finalPath);
    parts.path = finalPath;
  }

  if (query) {
    var queryString = Object.keys(query).map(function (k) {
      return k + '=' + query[k];
    }).join('&');
    if (scheme || host || path) queryString = '?' + queryString;
    parts.query = queryString;
  }

  // build uri from template adding non-empty parts only
  var pattern = '\\{(' + Object.keys(parts).join('|') + ')\\}',
      uri = template.replace(new RegExp(pattern, 'g'), function (_, p1) {
    return parts[p1];
  });
  return uri;
};