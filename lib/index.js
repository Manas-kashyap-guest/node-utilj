'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _uriBuild = require('./uriBuild');

Object.defineProperty(exports, 'uriBuild', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_uriBuild).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }